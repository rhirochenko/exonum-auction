import * as Exonum from 'exonum-client'
import bigInt from 'big-integer'
import axios from 'axios'

const PROTOCOL_VERSION = 0
const SERVICE_ID = 128
const CREATE_USER_TX_ID = 0
const ISSUE_TX_ID = 1
const CREATE_AUCTION_TX_ID = 2
const MAKE_LOT_TX_ID = 3
const MAKE_BID_TX_ID = 4

const ATTEMPTS = 10
const ATTEMPT_TIMEOUT = 500

const SystemTime = Exonum.newType({
  fields: [
    { name: 'secs', type: Exonum.Uint64 },
    { name: 'nanos', type: Exonum.Uint32 }
  ]
})
const Lot = Exonum.newType({
  fields: [
    { name: 'title', type: Exonum.String },
    { name: 'desc', type: Exonum.String },
  ]
})
const Auction = Exonum.newType({
  fields: [
    { name: 'public_key', type: Exonum.PublicKey },
    { name: 'lot_id', type: Exonum.Hash },
    { name: 'start_price', type: Exonum.Uint64 },
  ]
})

function getSystemTime() {
  const now = Date.now()
  const secs = bigInt(now).over(1000)
  const nanos = bigInt(now).minus(secs.multiply(1000)).multiply(1000000)

  return {
    secs: secs.toString(),
    nanos: nanos.valueOf()
  }
}

function waitForAcceptance(response) {
  let attempt = ATTEMPTS

  if (response.data.debug) {
    throw new Error(response.data.description)
  }

  return (function makeAttempt() {
    return axios.get(`/api/explorer/v1/transactions?hash=${response.data.tx_hash}`).then(response => {
      if (response.data.type === 'committed') {
        return response.data
      } else {
        if (--attempt > 0) {
          return new Promise((resolve) => {
            setTimeout(resolve, ATTEMPT_TIMEOUT)
          }).then(makeAttempt)
        } else {
          throw new Error('Transaction has not been found')
        }
      }
    })
  })()
}

module.exports = {
  install(Vue) {
    Vue.prototype.$blockchain = {
      createUser: name => {
        // Generate new signing key pair
        const keyPair = Exonum.keyPair()

        // Describe transaction to create new user
        const TxCreateUser = Exonum.newMessage({
          protocol_version: PROTOCOL_VERSION,
          service_id: SERVICE_ID,
          message_id: CREATE_USER_TX_ID,
          fields: [
            { name: 'public_key', type: Exonum.PublicKey },
            { name: 'name', type: Exonum.String }
          ]
        })

        // Transaction data
        const data = {
          public_key: keyPair.publicKey,
          name: name
        }

        // Sign transaction with user's secret key
        const signature = TxCreateUser.sign(keyPair.secretKey, data)
        TxCreateUser.signature = signature
        const hash = TxCreateUser.hash(data)
      
        // Send transaction into blockchain
        return TxCreateUser.send('/api/services/auction/v1/transaction', '/api/explorer/v1/transactions?hash=', data, signature)
        .then(() => keyPair)
       
      },

      makeLot: (keyPair, title, desc) => {
        const TxMakeLot = Exonum.newMessage({
          protocol_version: PROTOCOL_VERSION,
          service_id: SERVICE_ID,
          message_id: MAKE_LOT_TX_ID,
          fields: [
            { name: 'public_key', type: Exonum.PublicKey },
            { name: 'title', type: Exonum.String },
            { name: 'desc', type: Exonum.String },
            { name: 'seed', type: SystemTime }
          ]
        })

        // Transaction data
        const data = {
          public_key: keyPair.publicKey,
          title: title,
          desc: desc,
          seed: getSystemTime()
        }

        // Sign transaction with user's secret key
        const signature = TxMakeLot.sign(keyPair.secretKey, data)

        // Send transaction into blockchain
        return TxMakeLot.send('/api/services/auction/v1/transaction', '/api/explorer/v1/transactions?hash=', data, signature)
      },

      issue: (keyPair) => {
        // Describe transaction to issue funds
        const TxIssue = Exonum.newMessage({
          protocol_version: PROTOCOL_VERSION,
          service_id: SERVICE_ID,
          message_id: ISSUE_TX_ID,
          fields: [
            { name: 'public_key', type: Exonum.PublicKey },
            { name: 'seed', type: SystemTime }
          ]
        })

        // Transaction data
        const data = {
          public_key: keyPair.publicKey,
          seed: getSystemTime()
        }

        // Sign transaction with user's secret key
        const signature = TxIssue.sign(keyPair.secretKey, data)

        // Send transaction into blockchain
        return TxIssue.send('/api/services/auction/v1/transaction', '/api/explorer/v1/transactions?hash=', data, signature)
      },

      createAuction: (keyPair, lot_id, price) => {
        // Describe transaction to create auction
        const TxCreateAuction = Exonum.newMessage({
          protocol_version: PROTOCOL_VERSION,
          service_id: SERVICE_ID,
          message_id: CREATE_AUCTION_TX_ID,
          fields: [
            { name: 'public_key', type: Exonum.PublicKey },
            { name: 'lot_id', type: Exonum.Uint64 },
            { name: 'start_price', type: Exonum.Uint64 },
            { name: 'seed', type: SystemTime }
          ]
        })

        // Transaction data
        const data = {
          public_key: keyPair.publicKey,
          lot_id: lot_id,
          start_price: price,
          seed: getSystemTime(),
        }

        // Sign transaction with user's secret key
        const signature = TxCreateAuction.sign(keyPair.secretKey, data)

        // Send transaction into blockchain
        return TxCreateAuction.send('/api/services/auction/v1/transaction', '/api/explorer/v1/transactions?hash=', data, signature)
      },

      makeBid: (keyPair, auction, price) => {
        // Describe transaction to make bid
        const TxMakeBid = Exonum.newMessage({
          protocol_version: PROTOCOL_VERSION,
          service_id: SERVICE_ID,
          message_id: MAKE_BID_TX_ID,
          fields: [
            { name: 'public_key', type: Exonum.PublicKey },
            { name: 'auction_id', type: Exonum.Uint64 },
            { name: 'value', type: Exonum.Uint64 }
          ]
        })

        // Transaction data
        const data = {
          public_key: keyPair.publicKey,
          auction_id: auction,
          value: price
        }

        // Sign transaction with user's secret key
        const signature = TxMakeBid.sign(keyPair.secretKey, data)

        // Send transaction into blockchain
        return TxMakeBid.send('/api/services/auction/v1/transaction', '/api/explorer/v1/transactions?hash=', data, signature)
      },

      getUsers: () => {
        return axios.get('/api/services/auction/v1/users').then(response => response.data)
      },

      getUser: publicKey => {
        return axios.get(`/api/services/auction/v1/user?pub_key=${publicKey}`).then(response => {
          if (response.data === 'User not found') {
            throw new Error(response.data)
          }
          return response.data
        })
      },

      getAuctions:() => {
        return axios.get('/api/services/auction/v1/auctions').then(response => response.data)
      },

      getUserAuctions: publicKey => {
        return axios.get(`/api/services/auction/v1/user/auctions?pub_key=${publicKey}`).then(response => response.data)
      },

      getAuction: id => {
        return axios.get(`/api/services/auction/v1/auction?id=${id}`).then(response => response.data)
      },

      getBids: id => {
        return axios.get(`/api/services/auction/v1/auction/bids?id=${id}`).then(response => response.data)
      },

      getLots: () => {
        return axios.get('/api/services/auction/v1/lots').then(response => response.data)
      },

      getUserLots: publicKey => {
        return axios.get(`/api/services/auction/v1/user/lots?pub_key=${publicKey}`).then(response => response.data)
      },

      getLot: id => {
        return axios.get(`/api/services/auction/v1/lot?id=${id}`).then(response => response.data)
      },

      getBlocks: latest => {
        let suffix = !isNaN(latest) ? '&latest=' + latest : ''

        return axios.get(`/api/explorer/v1/blocks?count=10${suffix}`).then(response => response.data.blocks)
      },

      getBlock: height => {
        return axios.get(`/api/explorer/v1/block?height=${height}`).then(response => response.data)
      },

      getTransaction: hash => {
        return axios.get(`/api/explorer/v1/transactions?hash=${hash}`).then(response => response.data)
      }
    }
  }
}