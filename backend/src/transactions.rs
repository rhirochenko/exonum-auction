    //! Exonum Auctino transactions.

// Workaround for `failure` see https://github.com/rust-lang-nursery/failure/issues/223 and
// ECR-1771 for the details.
#![allow(bare_trait_objects)]

use chrono::{DateTime, Utc};
use exonum::{
    blockchain::{ExecutionError, ExecutionResult, Transaction},
    crypto::{Hash, PublicKey},
    messages::Message,
    storage::{Snapshot, Fork},
};
use exonum_time::schema::TimeSchema;

use schema::Schema;
use models::{User, Auction, AuctionInfo, Lot, LotInfo, Bid};
use AUCTION_SERVICE_ID;
use ISSUE_AMOUNT;

// A helper function to get current time from the time oracle.
pub fn current_time(snapshot: &Snapshot) -> Option<DateTime<Utc>> {
    let time_schema = TimeSchema::new(snapshot);
    time_schema.time().get()
}

/// Error codes emitted by wallet transactions during execution.
#[derive(Debug, Fail)]
#[repr(u8)]
pub enum Error {
    /// Custom error code.
    ///
    /// Can be emitted by `Transaction name`.
    #[fail(display = "User is already registered")]
    UserAlreadyRegistered = 1,

    #[fail(display = "User is not registered")]
    UserIsNotRegistered = 2,

    #[fail(display = "Insufficient funds.")]
    InsufficientFunds = 3,

    #[fail(display = "Not your property.")]
    AccessViolation = 4,

    #[fail(display = "Lot does not exist")]
    LotNotFound = 5,

    #[fail(display = "You do not own of the item")]
    LotNotOwned = 6,

    #[fail(display = "Lot is already auctioned")]
    LotAlreadyAuctioned = 7,

    #[fail(display = "Auction does not exist")]
    AuctionNotFound = 8,

    #[fail(display = "Auction is closed")]
    AuctionClosed = 9,

    #[fail(display = "Bid is below the current highest bid")]
    BidTooLow = 10,

    // CloseAuction can only be performed by the validator nodes.
    #[fail(display = "Transaction is not authorized.")]
    UnauthorizedTransaction = 11,

    #[fail(display = "You may not bid on your own item.")]
    NoSelfBidding = 12,
}

impl From<Error> for ExecutionError {
    fn from(value: Error) -> ExecutionError {
        let description = format!("{}", value);
        ExecutionError::with_description(value as u8, description)
    }
}

transactions! {
    /// Transaction group.
    pub AuctionTransactions {
        const SERVICE_ID = AUCTION_SERVICE_ID;

        /// Transaction to create a new user.
        struct CreateUser {
            /// Public key, used as identifier.
            public_key: &PublicKey,
            /// Name.
            name: &str,
        }

        /// Transaction to issue funds.
        struct Issue {
            /// Public user identifier.
            public_key: &PublicKey,
            /// Seed to avoid idempotence.
            seed: DateTime<Utc>,
        }

        /// Transaction to create new auction.
        struct CreateAuction {
            /// Auctioneer.
            public_key: &PublicKey,
            /// Lot with `lot_id` is auctioned.
            lot_id: u64,
            /// Start price.
            start_price: u64,
            /// Seed to avoid idempotence.
            seed: DateTime<Utc>,
        }

        /// Transaction to create an auction lot.
        struct MakeLot {
            /// Lot creator.
            public_key: &PublicKey,
            /// Lot title.
            title: &str,
            /// Lot description.
            desc: &str,
            /// Seed to avoid idempotence.
            seed: DateTime<Utc>,
        }

        struct MakeBid {
            /// Bidder.
            public_key: &PublicKey,
            /// Auction ID where a bid must be made.
            auction_id: u64,
            /// Bid value.
            value: u64,
        }
    }   
}

impl Transaction for CreateUser {
    fn verify(&self) -> bool {
        self.verify_signature(self.public_key())
    }

    fn execute(&self, fork: &mut Fork) -> ExecutionResult {
        let key = self.public_key();
        let mut schema = Schema::new(fork);

        // Reject tx if the user with the same public key is already exists.
        if schema.users().get(key).is_some() {
            Err(Error::UserAlreadyRegistered)?;
        }

        let user = User::new(key, self.name(), ISSUE_AMOUNT, 0);
        schema.users_mut().put(key, user);

        Ok(())
    }
}

impl Transaction for Issue {
    fn verify(&self) -> bool {
        self.verify_signature(self.public_key())
    }

    fn execute(&self, fork: &mut Fork) -> ExecutionResult {
        let mut schema = Schema::new(fork);
        let key = self.public_key();
        let user = schema.users().get(key).unwrap();

        schema.increase_user_balance(user.public_key(), ISSUE_AMOUNT);
        Ok(())
    }
}

impl Transaction for CreateAuction {
    fn verify(&self) -> bool {
        self.verify_signature(self.public_key())
    }

    fn execute(&self, fork: &mut Fork) -> ExecutionResult {
        let ts = current_time(fork).unwrap();

        let mut schema = Schema::new(fork);
        let auction_info = AuctionInfo::new(
            self.public_key(),
            self.lot_id(),
            self.start_price(),
        );

        // Check if the user is registered.
        let user = schema
            .users()
            .get(auction_info.auctioneer())
            .ok_or_else(|| Error::UserIsNotRegistered)?;

        // Check if the lot exists.
        let lot = schema
            .lots()
            .get(auction_info.lot_id())
            .ok_or_else(|| Error::LotNotFound)?;

        // Check if the user owns the lot.
        if lot.owner() != user.public_key() {
            Err(Error::LotNotOwned)?;
        }

        // Check if the lot isn't auctioned already.
        if schema.lot_auction().get(&auction_info.lot_id()).is_some() {
            Err(Error::LotAlreadyAuctioned)?;
        }

        // Establish a new auction.
        let auction_id = schema.auctions().len();
        let lot_id = auction_info.lot_id();
        let auction = Auction::new(auction_id, auction_info, ts, &Hash::zero(), false);

        schema.auctions_mut().push(auction);
        schema.lot_auction_mut().put(&lot_id, auction_id);
        schema.user_auctions_mut(user.public_key()).push(auction_id);

        Ok(())
    }
}

impl Transaction for MakeLot {
    fn verify(&self) -> bool {
        self.verify_signature(self.public_key())
    }

    fn execute(&self, fork: &mut Fork) -> ExecutionResult {
        let mut schema = Schema::new(fork);
        let lot_info = LotInfo::new(
            self.title(),
            self.desc(),
        );

        // Check if the user is registered.
        let user = schema
            .users()
            .get(self.public_key())
            .ok_or_else(|| Error::UserIsNotRegistered)?;

        // Add a new lot.
        let lot_id = schema.lots().len();
        let lot = Lot::new(lot_id, lot_info, self.public_key());

        schema.lots_mut().push(lot);
        schema.user_lots_mut(user.public_key()).push(lot_id);

        Ok(())
    }
}

impl Transaction for MakeBid {
    fn verify(&self) -> bool {
        self.verify_signature(self.public_key())
    }

    fn execute(&self, fork: &mut Fork) -> ExecutionResult {
        let mut schema = Schema::new(fork);

        // Check if the user is registered.
        let user = schema
            .users()
            .get(self.public_key())
            .ok_or_else(|| Error::UserIsNotRegistered)?;

        // Check if the auction exists.
        let auction = schema
            .auctions()
            .get(self.auction_id())
            .ok_or_else(|| Error::AuctionNotFound)?;

        let auction_info = auction.auction_info();

        // Check if the auction is open.
        if auction.closed() {
            Err(Error::AuctionClosed)?;
        }

        // Check if the user has enough funds.
        if user.balance() < self.value() {
            Err(Error::InsufficientFunds)?;
        }

        // Bidding in own auction is prohibited.
        if user.public_key() == auction_info.auctioneer() {
            Err(Error::NoSelfBidding)?;
        }

        // Get the bid to beat.
        let min_bid = match schema.bids(auction.id()).last() {
            Some(bid) => bid.value(),
            None => auction_info.start_price(),
        };

        // Check if the bid is higher than the min bid.
        if min_bid >= self.value() {
            Err(Error::BidTooLow)?;
        }

        // Release balance of the previous bidder if any.
        if let Some(b) = schema.bids(auction.id()).last() {
            let prev_bid_user = schema.users().get(b.bidder()).unwrap();
            schema.release_user_balance(prev_bid_user.public_key(), min_bid);
        }

        // Reserve value in user wallet.
        schema.reserve_user_balance(user.public_key(), self.value());

        // Make a bid.
        let bid = Bid::new(self.public_key(), self.value());
        schema.bids_mut(self.auction_id()).push(bid);

        // Refresh the auction state.
        let bids_merkle_root = schema.bids(self.auction_id()).merkle_root();
        schema.auctions_mut().set(
            auction.id(),
            Auction::new(
                auction.id(),
                auction_info,
                auction.started_at(),
                &bids_merkle_root,
                auction.closed(),
            ),
        );

        Ok(())
    }
}
