//! Exonum Auction database schema.
use exonum::{
    crypto::{Hash, PublicKey},
    storage::{Fork, MapIndex, ListIndex, ProofMapIndex, ProofListIndex, Snapshot},
};
use models::{User, Auction, Lot, Bid};

/// Database schema for the auction.
#[derive(Debug)]
pub struct Schema<T> {
    view: T,
}
impl<T> AsMut<T> for Schema<T> {
    fn as_mut(&mut self) -> &mut T {
        &mut self.view
    }
}

/// Snapshot methods.
impl<T> Schema<T> where T: AsRef<dyn Snapshot> {
    /// Creates a new schema from the database view.
    pub fn new(view: T) -> Self {
        Schema { view }
    }

    /// Users.
    pub fn users(&self) -> ProofMapIndex<&T, PublicKey, User> {
        ProofMapIndex::new("auction.users", &self.view)
    }

    /// Auctions.
    pub fn auctions(&self) -> ProofListIndex<&T, Auction> {
        ProofListIndex::new("auction.auctions", &self.view)
    }

    /// Auction Lots.
    pub fn lots(&self) -> ProofListIndex<&T, Lot> {
        ProofListIndex::new("auction.lots", &self.view)
    }

    /// Auction bids.
    pub fn bids(&self, auction_id: u64) -> ProofListIndex<&T, Bid> {
        ProofListIndex::new_in_family("auction.bids", &auction_id, &self.view)
    }

    /// Helper table for linking user and his lots.
    pub fn user_lots(&self, public_key: &PublicKey) -> ListIndex<&T, u64> {
        ListIndex::new_in_family("auction.user_lots", public_key, &self.view)
    }

    /// Helper table for linking user and his auctions.
    pub fn user_auctions(&self, public_key: &PublicKey) -> ListIndex<&T, u64> {
        ListIndex::new_in_family("auction.user_auctions", public_key, &self.view)
    }

    /// Helper table for linking lot and its open auction.
    pub fn lot_auction(&self) -> MapIndex<&T, u64, u64> {
        MapIndex::new("auction.lot_auction", &self.view)
    }

    /// Returns the state hash of cryptocurrency service.
    /// Depends on `users`, `auctions` and `lots` tables.
    pub fn state_hash(&self) -> Vec<Hash> {
        vec![
            self.users().merkle_root(),
            self.auctions().merkle_root(),
            self.lots().merkle_root(),
        ]
    }
}

/// Implementation of mutable methods.
impl<'a> Schema<&'a mut Fork> {
    pub fn users_mut(&mut self) -> ProofMapIndex<&mut Fork, PublicKey, User> {
        ProofMapIndex::new("auction.users", self.view)
    }

    pub fn auctions_mut(&mut self) -> ProofListIndex<&mut Fork, Auction> {
        ProofListIndex::new("auction.auctions", self.view)
    }

    pub fn lots_mut(&mut self) -> ProofListIndex<&mut Fork, Lot> {
        ProofListIndex::new("auction.lots", self.view)
    }

    pub fn bids_mut(&mut self, auction_id: u64) -> ProofListIndex<&mut Fork, Bid> {
        ProofListIndex::new_in_family("auction.bids", &auction_id, self.view)
    }

    pub fn user_lots_mut(&mut self, public_key: &PublicKey) -> ListIndex<&mut Fork, u64> {
        ListIndex::new_in_family("auction.user_lots", public_key, self.view)
    }

    pub fn user_auctions_mut(&mut self, public_key: &PublicKey) -> ListIndex<&mut Fork, u64> {
        ListIndex::new_in_family("auction.user_auctions", public_key, self.view)
    }

    pub fn lot_auction_mut(&mut self) -> MapIndex<&mut Fork, u64, u64> {
        MapIndex::new("auction.lot_auction", self.view)
    }
}

/// Helper methods.
impl<T> Schema<T> where T: AsRef<dyn Snapshot> {
    // No implementation required.
}

/// Mutable helper methods.
impl<'a> Schema<&'a mut Fork> {
    /// Helper method to increase user balance.
    pub fn increase_user_balance(&mut self, user_id: &PublicKey, balance: u64) {
        let user = self.users().get(user_id).expect("User should be exist.");
        self.users_mut().put(
            user.public_key(),
            User::new(
                user.public_key(),
                user.name(),
                user.balance() + balance,
                user.reserved(),
            ),
        );
    }

    /// Helper method to decrease user balance.
    pub fn decrease_user_balance(&mut self, user_id: &PublicKey, balance: u64) {
        let user = self.users().get(user_id).expect("User should be exist.");
        self.users_mut().put(
            user.public_key(),
            User::new(
                user.public_key(),
                user.name(),
                user.balance() - balance,
                user.reserved(),
            ),
        );
    }

    /// Helper method to decrease user reserved balance.
    pub fn reserve_user_balance(&mut self, user_id: &PublicKey, reserve: u64) {
        let user = self.users().get(user_id).expect("User should be exist.");
        self.users_mut().put(
            user.public_key(),
            User::new(
                user.public_key(),
                user.name(),
                user.balance() - reserve,
                user.reserved() + reserve,
            ),
        );
    }

    /// Helper method to decrease user reserved balance.
    pub fn release_user_balance(&mut self, user_id: &PublicKey, reserve: u64) {
        let user = self.users().get(user_id).expect("User should be exist.");
        self.users_mut().put(
            user.public_key(),
            User::new(
                user.public_key(),
                user.name(),
                user.balance() + reserve,
                user.reserved() - reserve,
            ),
        );
    }

    /// Helper method to decrease user bid with value.
    pub fn confirm_user_bid(&mut self, user_id: &PublicKey, bid_value: u64) {
        let user = self.users().get(user_id).expect("User should be exist.");
        self.users_mut().put(
            user.public_key(),
            User::new(
                user.public_key(),
                user.name(),
                user.balance(),
                user.reserved() - bid_value,
            ),
        );
    }
}
