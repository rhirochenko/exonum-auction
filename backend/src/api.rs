//! Exonum Auction API.

use exonum::{
    api::{self, ServiceApiBuilder, ServiceApiState},
    blockchain::{Transaction},
    crypto::{Hash, PublicKey},
    node::TransactionSend,
};

use transactions::AuctionTransactions;
use models::{User, Auction, Lot, Bid};
use schema::Schema;

/// Auction service API description.
#[derive(Debug)]
pub struct AuctionApi;

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct UserQuery {
    pub pub_key: PublicKey,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct LotQuery {
    pub id: u64,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct AuctionQuery {
    pub id: u64,
}


impl AuctionApi {
    /// User profile.
    fn get_user(state: &ServiceApiState, query: UserQuery)
    -> api::Result<Option<User>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);
        Ok(schema.users().get(&query.pub_key))
    }

    /// All users.
    fn get_users(state: &ServiceApiState, _query: ())
    -> api::Result<Vec<User>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);
        let idx = schema.users();
        let users: Vec<User> = idx.values().collect();
        Ok(users)
    }

    /// Auction Lot.
    fn get_lot(state: &ServiceApiState, query: LotQuery)
    -> api::Result<Option<Lot>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);
        Ok(schema.lots().get(query.id))
    }

    /// All auction lots.
    fn get_lots(state: &ServiceApiState, _query: ())
    -> api::Result<Vec<Lot>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        let idx = schema.lots();
        let lots = idx.into_iter().collect::<Vec<_>>();
        Ok(lots)
    }

    /// User lots list.
    fn get_user_lots(state: &ServiceApiState, query: UserQuery)
    -> api::Result<Option<Vec<Lot>>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        Ok(schema.users().get(&query.pub_key).map(|user| {
            let user_lots = schema.user_lots(user.public_key());
            let lots = user_lots
                .into_iter()
                .map(|lot_id| schema.lots().get(lot_id).unwrap())
                .collect();
            lots
        }))
    }

    /// Auctions made by user.
    fn get_users_auctions(state: &ServiceApiState, query: UserQuery)
    -> api::Result<Option<Vec<Auction>>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        Ok(schema.users().get(&query.pub_key).map(|user| {
            let user_auctions = schema.user_auctions(user.public_key());
            let auctions = user_auctions
                .into_iter()
                .map(|auction_id| schema.auctions().get(auction_id).unwrap())
                .collect();
            auctions
        }))
    }

    /// Auctions and bids by auction identifier.
    fn get_auction_with_bids(state: &ServiceApiState, query: AuctionQuery)
    -> api::Result<Option<(Auction, Vec<Bid>)>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        Ok(schema
            .auctions()
            .get(query.id)
            .map(|auction| {
                let bids = schema.bids(auction.id());
                let bids = bids.into_iter().collect();
                (auction, bids)
            }))
    }

    /// Auction bids by its identifier.
    fn get_auction_bids(state: &ServiceApiState, query: AuctionQuery)
    -> api::Result<Option<Vec<Bid>>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);

        Ok(schema
            .auctions()
            .get(query.id)
            .map(|auction| {
                let bids = schema.bids(auction.id());
                let bids = bids.into_iter().collect();
                bids
            }))
    }

    /// All auctions.
    fn get_auctions(state: &ServiceApiState, _query: ())
    -> api::Result<Vec<Auction>> {
        let snapshot = state.snapshot();
        let schema = Schema::new(snapshot);
        let auctions = schema.auctions();
        let auctions = auctions.into_iter().collect::<Vec<_>>();
        Ok(auctions)
    }

    /// Endpoint for handling cryptocurrency transactions.
    fn post_transaction(state: &ServiceApiState, query: AuctionTransactions)
    -> api::Result<Hash> {
        let transaction: Box<dyn Transaction> = query.into();
        let tx_hash = transaction.hash();
        state.sender().send(transaction)?;
        Ok(tx_hash)
    }

    /// Wires the above endpoint to public scope of the given `ServiceApiBuilder`.
    pub fn wire(builder: &mut ServiceApiBuilder) {
        builder
            .public_scope()
            .endpoint("v1/users", Self::get_users)
            .endpoint("v1/user", Self::get_user)
            .endpoint("v1/user/lots", Self::get_user_lots)
            .endpoint("v1/user/auctions", Self::get_users_auctions)
            .endpoint("v1/auctions", Self::get_auctions)
            .endpoint("v1/auction", Self::get_auction_with_bids)
            .endpoint("v1/auction/bids", Self::get_auction_bids)
            .endpoint("v1/lots", Self::get_lots)
            .endpoint("v1/lot", Self::get_lot)
            .endpoint_mut("v1/transaction", Self::post_transaction);
    }
}
