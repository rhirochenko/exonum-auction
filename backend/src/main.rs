extern crate exonum;
extern crate exonum_configuration;
extern crate exonum_auction;
extern crate exonum_time;

use exonum::helpers::{self, fabric::NodeBuilder};
use exonum_configuration as configuration;
use exonum_auction as auction;
use exonum_time::TimeServiceFactory;

fn main() {
    exonum::crypto::init();
    helpers::init_logger().unwrap();

    let node = NodeBuilder::new()
        .with_service(Box::new(configuration::ServiceFactory))
        .with_service(Box::new(TimeServiceFactory))
        .with_service(Box::new(auction::ServiceFactory));
    node.run();
}
